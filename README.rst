=====
Tools for Storytelling with Data
=====
Description
--------
Part of Love Data Week 2018, hosted by Data Services at NYU Libraries, this workshop will offer hands-on instruction in publishing code and data using notebook tools such as Jupyter Notebooks and R Markdown. We'll look at some of the features of these notebooks, how to approach telling a story by making "executable research papers" on these platforms, and how to push them to the web.
Although this workshop assumes you are producing some kind of data workflow in R or Python, participants are welcomed regardless of their experience in these languages. The focus of the workshop will be on working with these notebooks, not on writing code. Register here: https://nyu.libcal.com/event/3973328

Location & Date/Time
--------
Tuesday February 13th, 3-4:30pm

Bobst Library, room 743

Topics
---------
Jupyter notebooks, R, Python, R Markdown, publishing, gitlab, github
